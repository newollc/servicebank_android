package com.servicebank.app;

import android.app.Application;
import android.content.res.Configuration;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.servicebank.app.helper.SharedPreferenceHelper;

public class ServiceBankApplication extends Application {

    private SharedPreferenceHelper sharedPreferenceHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        sharedPreferenceHelper = new SharedPreferenceHelper(this);
        getToken();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void setNotificationToken(String token) {
        sharedPreferenceHelper.setToken(token);
    }

    public String getNotificationToken() {
        return sharedPreferenceHelper.getToken();
    }

    private void getToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();
                setNotificationToken(deviceToken);
            }
        });
    }
}
