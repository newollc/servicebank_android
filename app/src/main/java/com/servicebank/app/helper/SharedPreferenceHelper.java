package com.servicebank.app.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.servicebank.app.util.Constant;

import java.lang.annotation.Target;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferenceHelper {

    private static final String TAG = "SharedPreferenceHelper";

    private SharedPreferences prefs;

    public SharedPreferenceHelper(Context context) {
        prefs = context.getSharedPreferences(context.getPackageName(), MODE_PRIVATE);
    }

    public void setToken(String token) {
        Log.d(TAG, token);
        prefs.edit().putString(Constant.COOKIE_KEY_SESSION, token).commit();
    }

    public String getToken() {
        String token = prefs.getString(Constant.COOKIE_KEY_SESSION, " ");
        Log.d(TAG, token);
        return token;
    }
}
