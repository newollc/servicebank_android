package com.servicebank.app.util;

public class Constant {

    public static String URL_WEBSITE = "https://app.servicebank.com/";
    public static String URL_GET_AUTH = "https://app.servicebank.com/verify-auth";
    public static String URL_SET_TOKEN = "https://app.servicebank.com/firebase-token";

    public static String KEY_URL = "url";

    public static String URL_KEY_TOKEN = "token";
    public static String COOKIE_KEY_SESSION = "servicebank_session";
}
