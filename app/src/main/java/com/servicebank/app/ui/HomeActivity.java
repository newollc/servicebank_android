package com.servicebank.app.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.servicebank.app.R;
import com.servicebank.app.ServiceBankApplication;
import com.servicebank.app.util.NetworkConnectionUtil;
import com.servicebank.app.util.Constant;

import org.riversun.okhttp3.OkHttp3CookieHelper;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = "HomeActivity";

    private WebView mWebView;
    private RelativeLayout rlSplash;
    private AppCompatImageView ivSplashLogo;
    private AppCompatButton btnRefresh;

    private CookieManager mCookieManager;
    private String url = Constant.URL_WEBSITE;
    private String mCurrentUrl = " ";
    private ServiceBankApplication application;
    private NetworkConnectionUtil ncu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        application = (ServiceBankApplication) getApplication();
        ncu = new NetworkConnectionUtil(this);

        getUrlFromIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getUrlFromIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void getUrlFromIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey(Constant.KEY_URL)) {
                url = extras.getString(Constant.KEY_URL);
            }
        }

        setUi();
    }

    private void setUi() {
        rlSplash = findViewById(R.id.rl_splash);
        ivSplashLogo = findViewById(R.id.image_view_splash_logo);
        mWebView = findViewById(R.id.web_view_home);
        btnRefresh = findViewById(R.id.btn_refresh);
        btnRefresh.setOnClickListener(this);

        loadLogo();
        showSplash();
        setWebView();
    }

    private void loadLogo() {
//        Glide.with(this).load(R.drawable.logo_servicebank).into(ivSplashLogo);
    }

    private void showSplash() {
        rlSplash.setVisibility(View.VISIBLE);
    }

    private void hideSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rlSplash.setVisibility(View.GONE);
            }
        }, 5000);
    }

    private void setWebView() {
        mWebView.setWebViewClient(new ServiceBankWebViewClient());
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.setInitialScale(1);

        mCookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(this);
        }
        mCookieManager.setAcceptCookie(true);
        mCookieManager.acceptCookie();
        mCookieManager.acceptThirdPartyCookies(mWebView);


        mWebView.loadUrl(url);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mWebView.canGoBack()) {
                        mWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    public void verifySession() {
        OkHttp3CookieHelper cookieHelper = new OkHttp3CookieHelper();
        cookieHelper.setCookie(mCurrentUrl, Constant.COOKIE_KEY_SESSION, getCookie(mCurrentUrl, Constant.COOKIE_KEY_SESSION));

        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(cookieHelper.cookieJar())
                .build();

        RequestBody formBody = new FormBody.Builder()
                .build();

        Request request = new Request.Builder()
                .url(Constant.URL_GET_AUTH)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.w(TAG, response.body().string());
                Log.i(TAG, response.toString());
                sendToken(application.getNotificationToken());
            }
        });
    }

    public void sendToken(String token) {
        OkHttp3CookieHelper cookieHelper = new OkHttp3CookieHelper();
        cookieHelper.setCookie(mCurrentUrl, Constant.COOKIE_KEY_SESSION, getCookie(mCurrentUrl, Constant.COOKIE_KEY_SESSION));

        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(cookieHelper.cookieJar())
                .build();

        RequestBody formBody = new FormBody.Builder()
                .add(Constant.URL_KEY_TOKEN, token)
                .build();

        Request request = new Request.Builder()
                .url(Constant.URL_SET_TOKEN)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.w(TAG, response.body().string());
                Log.i(TAG, response.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_refresh:
                mWebView.reload();
                break;
        }
    }

    class ServiceBankWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stubsuper.onPageStarted(view, url, favicon);
            if (isConnectingToInternet()) {
                mWebView.setVisibility(View.VISIBLE);
                Log.d("WebView", "Loaoding");
            } else {
                mWebView.setVisibility(View.GONE);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            if (url.contains(Constant.URL_WEBSITE)) {
                view.loadUrl(url);
            } else {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mCurrentUrl = url;
            mCookieManager.acceptCookie();
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.getInstance().sync();
            }
            hideSplash();
            verifySession();
        }
    }

    public String getCookie(String url, String CookieName) {
        String cookieValue = " ";

        String cookies = mCookieManager.getCookie(url);
        Log.d(HomeActivity.TAG, cookies);
        if(cookies != null) {
            String[] temp = cookies.split(";");
            for (String ar1 : temp) {
                if (ar1.contains(CookieName)) {
                    String[] temp1 = ar1.split("=");
                    cookieValue = temp1[1];
                    break;
                }
            }
        }
        return cookieValue;
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                //Toast.makeText(_context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();

                return true;

            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                //connected to Data
                //Toast.makeText(_context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();

                return true;
            }
        } else {
            // not connected to the internet
        }
        return false;
    }
}

